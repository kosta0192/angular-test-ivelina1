import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    cpassword: new FormControl('')
  });

  collectData(): void {
    console.log(this.registerForm.value);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
